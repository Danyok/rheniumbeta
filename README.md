# Application Template

WolframAlpha application for Aurora OS.
Forked by sailwalpha https://github.com/TeemuAhola/sailwalpha
Application was ported on Aurora OS, name changed and Russian language added.
Don't forget use your own WolframAlpha API ID!!!

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

Application based on C++, Python and QML for Aurora OS.
